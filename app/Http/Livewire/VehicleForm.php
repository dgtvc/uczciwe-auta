<?php

namespace App\Http\Livewire;

use App\Models\Vehicle;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class VehicleForm extends Component implements HasForms
{
    use InteractsWithForms;

    private ?Vehicle $vehicle;

    public function edit()
    {
        $validated = $this->validate();

        $this->vehicle->update($validated);

        return redirect()->route('dashboard.vehicle.index');
    }

    public function mount(?Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;

        if (is_null($vehicle)) {
            $this->vehicle = new Vehicle();
        }

        $this->form->fill([
            'brand' => $this->vehicle->brand,
            'model' => $this->vehicle->model,
            'price' => $this->vehicle->price,
        ]);
    }

    public function render(): View
    {
        return view('livewire.vehicle-form');
    }

    public function store()
    {
        $validated = $this->validate();

        Vehicle::create($validated);

        return redirect()->route('dashboard.vehicle.edit')->with('success', 'Updated successfully');
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updated(string $propertyName)
    {
        $this->validateOnly($propertyName);
    }

    protected function getFormSchema(): array
    {
        return [
            TextInput::make('brand')->required()->minValue(3)->label('Marka'),
            TextInput::make('model')->required()->minValue(3),
            TextInput::make('price')->required()->numeric()->minValue(1)->maxValue(99999),
//            MarkdownEditor::make('description')->required(),
            // ...
        ];
    }
}
