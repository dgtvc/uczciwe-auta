<?php

namespace App\Http\Livewire\TableView;

use App\Actions\EditVehicleAction;
use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Builder;
use LaravelViews\Actions\RedirectAction;
use LaravelViews\Facades\Header;
use LaravelViews\Views\TableView;

class VehicleTableView extends TableView
{
    protected $paginate = 10;

    public $searchBy = [
        'brand',
        'model',
    ];

    protected function repository(): Builder
    {
        return Vehicle::query();
    }

    /**
     * Sets the headers of the table as you want to be displayed
     *
     * @return array<string> Array of headers
     */
    public function headers(): array
    {
        return [
            Header::title('ID')->sortBy('id'),
            Header::title('Marka')->sortBy('brand'),
            Header::title('Model')->sortBy('model'),
            Header::title('Cena')->sortBy('price'),
            Header::title('Created')->sortBy('created_at'),
        ];
    }

    /**
     * Sets the data to every cell of a single row
     */
    public function row(Vehicle $vehicle): array
    {
        return [
            $vehicle->id,
            $vehicle->brand,
            $vehicle->model,
            $vehicle->price,
            $vehicle->created_at->diffforHumans()
        ];
    }

    protected function actionsByRow(): array
    {
        return [
            new RedirectAction('dashboard.vehicle.edit', 'Edytuj', 'edit-2'),
        ];
    }
}


