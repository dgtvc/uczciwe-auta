<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use Illuminate\Contracts\View\View;

class VehicleController extends AbstractController
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        return view('pages.vehicle.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        return view('pages.vehicle.show', ['vehicle' => $vehicle]);
    }
}
