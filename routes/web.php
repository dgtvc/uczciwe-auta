<?php

use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Dashboard\VehicleController as DashboardVehicleController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Dashboard\SettingsController;
use App\Http\Controllers\VehicleController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', HomeController::class)->name('home');

Auth::routes([
    'register' => false,
    'verify' => false,
]);

Route::name('vehicle.')->prefix('auta')->group(function () {
    Route::get('', [VehicleController::class, 'index'])->name('index');
    Route::get('{vehicle}', [VehicleController::class, 'show'])->name('show');
});

Route::name( 'dashboard.')->middleware('auth')->prefix('dashboard')->group(function () {
    Route::get('/', DashboardController::class)->name('index');
    Route::get('settings', [SettingsController::class, 'show'])->name('settings');
    Route::put('settings', [SettingsController::class, 'update'])->name('settings.store');
    Route::resource('vehicle', DashboardVehicleController::class);
});
