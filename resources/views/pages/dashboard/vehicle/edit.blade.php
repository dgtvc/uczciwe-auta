<x-layouts.app :title="__('Dashboard')">
    <main>
        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 mt-8">
            <div class="sm:grid sm:grid-cols-1 sm:gap-10">
                <div>
                    <h2 class="text-2xl font-bold leading-tight text-gray-900">
                        Edytujesz: {{ $vehicle->brand }} - {{ $vehicle->model }}
                    </h2>
                    <div class="mt-6">
                        <livewire:vehicle-form :vehicle="$vehicle"/>
                    </div>
                </div>
            </div>
        </div>
    </main>
</x-layouts.app>
