<x-layouts.base>
    <div class="relative bg-white overflow-hidden">
        <div class="max-w-screen-xl mx-auto">
            <div class="relative z-10 pb-8 bg-white sm:pb-16 md:pb-20 lg:max-w-2xl lg:w-full lg:pb-28 xl:pb-32"
                 x-data="{ open: false }">
                <svg class="hidden lg:block absolute right-0 inset-y-0 h-full w-48 text-white transform translate-x-1/2"
                     fill="currentColor" viewBox="0 0 100 100" preserveAspectRatio="none">
                    <polygon points="50,0 100,0 50,100 0,100"/>
                </svg>

                <div class="relative pt-6 px-4 sm:px-6 lg:px-8">
                    <nav class="relative flex items-center justify-between sm:h-10 lg:justify-start">
                        <div class="flex items-center flex-grow flex-shrink-0 lg:flex-grow-0">
                            <div class="flex items-center justify-between w-full md:w-auto">
                                <a href="{{ route('home') }}" class="hover:zoom-1">
                                    <svg width="90" height="90" viewBox="0 0 100 100"
                                         class="h-8 w-auto sm:h-10 text-indigo-600">
                                        <defs>
                                            <linearGradient id="SvgjsLinearGradient1011">
                                                <stop id="SvgjsStop1012" stop-color="#7d141d" offset="0"></stop>
                                                <stop id="SvgjsStop1013" stop-color="#ff1e27" offset="1"></stop>
                                            </linearGradient>
                                        </defs>
                                        <g id="SvgjsG1007" featurekey="S6ay6y-0"
                                           transform="matrix(1.0377857677696227,0,0,1.0377857677696227,-1.7030064785600785,-6.414553577218372)"
                                           fill="url(#SvgjsLinearGradient1011)">
                                            <g xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M53.591,95C29.104,95,9.182,75.076,9.182,50.59c0-24.487,19.923-44.409,44.409-44.409C78.079,6.181,98,26.103,98,50.59   C98,75.076,78.079,95,53.591,95z M53.591,11.616c-21.49,0-38.974,17.483-38.974,38.974s17.484,38.975,38.974,38.975   c21.491,0,38.974-17.484,38.974-38.975S75.082,11.616,53.591,11.616z"></path>
                                                <path
                                                    d="M53.591,29.009c-11.899,0-21.581,9.682-21.581,21.581c0,11.9,9.682,21.583,21.581,21.583   c11.9,0,21.582-9.683,21.582-21.583C75.173,38.69,65.491,29.009,53.591,29.009z M53.591,34.958c1.166,0,2.112,0.947,2.112,2.113   c0,1.166-0.946,2.112-2.112,2.112c-1.168,0-2.112-0.947-2.112-2.112C51.479,35.904,52.423,34.958,53.591,34.958z M40.07,52.703   c-1.165,0-2.112-0.947-2.112-2.113c0-1.167,0.947-2.112,2.112-2.112c1.169,0,2.113,0.945,2.113,2.112   C42.184,51.756,41.239,52.703,40.07,52.703z M53.591,66.223c-1.168,0-2.112-0.946-2.112-2.112c0-1.168,0.944-2.112,2.112-2.112   c1.166,0,2.112,0.944,2.112,2.112C55.703,65.276,54.757,66.223,53.591,66.223z M53.591,56.684c-3.365,0-6.094-2.727-6.094-6.094   c0-3.369,2.729-6.093,6.094-6.093c3.366,0,6.093,2.724,6.093,6.093C59.684,53.957,56.957,56.684,53.591,56.684z M67.11,52.703   c-1.165,0-2.111-0.947-2.111-2.113c0-1.167,0.946-2.112,2.111-2.112c1.166,0,2.113,0.945,2.113,2.112   C69.224,51.756,68.276,52.703,67.11,52.703z"></path>
                                                <path
                                                    d="M30.463,91.83h-7.794l-0.549-0.409C9.106,81.671,1.641,66.788,1.641,50.59c0-16.198,7.465-31.08,20.479-40.83l0.549-0.41   h7.794l-0.042,23.451l-1.223,5.51c-2.011,3.833-3.03,7.964-3.03,12.278c0,4.314,1.019,8.446,3.03,12.278l0.197,0.561l1.068,5.626   V91.83z"></path>
                                            </g>
                                        </g>
                                        <g id="SvgjsG1008" featurekey="j5pGhi-0"
                                           transform="matrix(1.5274466567176912,0,0,1.5274466567176912,117.25533123277177,28.39226543674146)"
                                           fill="#ffffff">
                                            <path
                                                d="M1.7969 4.707000000000001 l1.4453 0 l0 8.7402 q0 1.552734375 0.068359375 1.943359375 q0.107421875 0.859375 0.498046875 1.430664063 t1.2061 0.95703 t1.6455 0.38574 q0.712890625 0.185546875 1.962890625 -0.126953125 t1.6895 -0.93262 t0.64453 -1.4014 q0.15625 -0.546875 0.15625 -2.255859375 l0 -8.7402 l1.4453 0 l0 8.7402 q0 1.943359375 -0.380859375 3.139648438 t-1.5186 2.0801 t-3.9209 0.88379 q-1.7578125 0 -3.002929688 -0.83984375 t-1.665 -2.207 q-0.2734375 -0.859375 -0.2734375 -3.056640625 l0 -8.7402 z M26.074390625 10.7031 l-1.0938 0.68359 q-1.416015625 -1.89453125 -3.8671875 -1.89453125 q-1.962890625 0 -3.256835938 1.259765625 t-1.2939 3.0664 q0 1.171875 0.595703125 2.211914063 t1.6309 1.6064 t2.3242 0.56641 q2.373046875 0 3.8671875 -1.875 l1.0938 0.71289 q-0.771484375 1.15234375 -2.065429688 1.782226563 t-2.9541 0.62988 q-2.529296875 0 -4.204101563 -1.611328125 t-1.6748 -3.916 q0 -1.5625 0.7861328125 -2.895507813 t2.1484 -2.0801 t3.0615 -0.74707 q1.0546875 0 2.045898438 0.322265625 t1.6846 0.84473 t1.1719 1.333 z M27.841794375 8.613 l7.8613 0 l-6.0742 9.6094 l5.8789 0 l0 1.2207 l-8.2422 0 l6.0547 -9.6191 l-5.4785 0 l0 -1.2109 z M47.6661875 10.7031 l-1.0938 0.68359 q-1.416015625 -1.89453125 -3.8671875 -1.89453125 q-1.962890625 0 -3.256835938 1.259765625 t-1.2939 3.0664 q0 1.171875 0.595703125 2.211914063 t1.6309 1.6064 t2.3242 0.56641 q2.373046875 0 3.8671875 -1.875 l1.0938 0.71289 q-0.771484375 1.15234375 -2.065429688 1.782226563 t-2.9541 0.62988 q-2.529296875 0 -4.204101563 -1.611328125 t-1.6748 -3.916 q0 -1.5625 0.7861328125 -2.895507813 t2.1484 -2.0801 t3.0615 -0.74707 q1.0546875 0 2.045898438 0.322265625 t1.6846 0.84473 t1.1719 1.333 z M50.78123125 4.3260000000000005 q0.46875 0 0.8056640625 0.33203125 t0.33691 0.80078 t-0.33691 0.80566 t-0.80566 0.33691 t-0.80078 -0.33691 t-0.33203 -0.80566 t0.33203 -0.80078 t0.80078 -0.33203 z M50.09763125 8.73 l1.377 0 l0 10.713 l-1.377 0 l0 -10.713 z M52.88085975 8.75 l1.416 0 l3.2813 7.7832 l3.4375 -7.7832 l0.24414 0 l3.418 7.7832 l3.3398 -7.7832 l1.4453 0 l-4.6484 10.713 l-0.27344 0 l-3.3984 -7.666 l-3.4375 7.666 l-0.25391 0 z M80.088015625 15.6348 l1.1523 0.61523 q-0.556640625 1.123046875 -1.30859375 1.806640625 t-1.6895 1.04 t-2.1191 0.35645 q-2.6171875 0 -4.091796875 -1.713867188 t-1.4746 -3.8721 q0 -2.041015625 1.25 -3.6328125 q1.58203125 -2.03125 4.248046875 -2.03125 q2.724609375 0 4.365234375 2.080078125 q1.162109375 1.46484375 1.171875 3.65234375 l-9.6191 0 q0.0390625 1.875 1.19140625 3.071289063 t2.8418 1.1963 q0.8203125 0 1.591796875 -0.2880859375 t1.3135 -0.75684 t1.1768 -1.5234 z M80.088015625 12.7539 q-0.2734375 -1.103515625 -0.80078125 -1.762695313 t-1.3965 -1.0645 t-1.8262 -0.40527 q-1.572265625 0 -2.705078125 1.015625 q-0.8203125 0.7421875 -1.240234375 2.216796875 l7.9688 0 z M94.57033125 4.901999999999999 l6.748 14.482 l-1.543 0 l-2.2754 -4.7754 l-6.25 0 l-2.2559 4.7754 l-1.6211 0 l6.8457 -14.482 l0.35156 0 z M94.39453125 7.968999999999999 l-2.5 5.2539 l4.9609 0 z M103.154340625 8.477 l1.3867 0 l0 5 q0 1.8359375 0.1953125 2.51953125 q0.29296875 0.986328125 1.123046875 1.557617188 t1.9922 0.57129 q1.15234375 0 1.953125 -0.556640625 t1.1035 -1.4648 q0.21484375 -0.615234375 0.21484375 -2.626953125 l0 -5 l1.4063 0 l0 5.2637 q0 2.216796875 -0.5224609375 3.334960938 t-1.5576 1.748 t-2.5977 0.62988 q-1.572265625 0 -2.6171875 -0.6298828125 t-1.5625 -1.7676 t-0.51758 -3.4229 l0 -5.1563 z M119.43356875 17.9297 q-1.015625 0.546875 -1.9921875 0.087890625 q-1.19140625 -0.908203125 -1.1328125 -2.587890625 l0 -4.5313 l2.5391 0 l0 -1.1816 l-2.5488 0 l0 -5.1758 l-1.3867 0 l0 11.123 q-0.01953125 1.171875 0.537109375 2.1484375 q0.91796875 1.552734375 2.5 1.640625 q0.83984375 0.029296875 1.9140625 -0.380859375 q-0.166015625 -0.41015625 -0.4296875 -1.142578125 z M127.11914375 8.203 q2.470703125 0 4.1015625 1.787109375 q1.50390625 1.630859375 1.50390625 3.876953125 q0.01953125 5.546875 0 5.634765625 l-1.4551 0 l0 -2.4316 q-1.142578125 2.12890625 -4.150390625 2.3828125 q-2.4609375 0 -4.018554688 -1.674804688 t-1.5576 -3.9111 q0 -2.2265625 1.474609375 -3.8671875 q1.630859375 -1.796875 4.1015625 -1.796875 z M127.11914375 9.551 q-1.708984375 0 -2.939453125 1.26953125 t-1.2305 3.0664 q0 1.162109375 0.6298828125 2.236328125 t2.1338 1.8164 q3.76953125 0.83984375 5.361328125 -3.427734375 q0.087890625 -1.3671875 -0.29296875 -2.6171875 q-0.2734375 -0.56640625 -0.751953125 -1.07421875 q-1.201171875 -1.26953125 -2.91015625 -1.26953125 z"></path>
                                        </g>
                                    </svg>
                                </a>
                                <div class="-mr-2 flex items-center md:hidden">
                                    <button type="button"
                                            class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out"
                                            @click="open = true">
                                        <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                  d="M4 6h16M4 12h16M4 18h16"/>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="hidden md:block md:ml-10 md:pr-4">
                            <a href="{{ route('home') }}"
                               class="font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 transition duration-150 ease-in-out">
                                {{ config('app.name') }}
                            </a>
                            <a href="#"
                               class="ml-8 font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 transition duration-150 ease-in-out">
                                Auta
                            </a>
                            <a href="#"
                               class="ml-8 font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 transition duration-150 ease-in-out">
                                O nas
                            </a>

                            @auth
                                <a href="{{ route('dashboard.index') }}"
                                   class="ml-8 font-medium text-indigo-600 hover:text-indigo-900 focus:outline-none focus:text-indigo-700 transition duration-150 ease-in-out">
                                    {{ __('Panel') }}
                                </a>
                            @else
                                <a href="{{ route('login') }}"
                                   class="ml-8 font-medium text-indigo-600 hover:text-indigo-900 focus:outline-none focus:text-indigo-700 transition duration-150 ease-in-out">
                                    {{ __('Login') }}
                                </a>
                            @endauth
                        </div>
                    </nav>
                </div>

                <div
                    x-show="open" @click.away="open = false"
                    class="absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden"
                >
                    <div class="rounded-lg shadow-md">
                        <div class="rounded-lg bg-white shadow-xs overflow-hidden">
                            <div class="px-5 pt-4 flex items-center justify-between">
                                <div>
                                    <x-heroicon-o-globe class="h-8 w-auto sm:h-10 text-indigo-600"/>
                                </div>
                                <div class="-mr-2" @click="open = false">
                                    <button type="button"
                                            class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                                        <x-heroicon-o-x class="h-6 w-6"/>
                                    </button>
                                </div>
                            </div>

                            <div class="px-2 pt-2 pb-3">
                                <a href="{{ route('home') }}"
                                   class="block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out">
                                    {{ config('app.name') }}
                                </a>
                                <a href="#"
                                   class="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out">
                                    Destinations
                                </a>
                                <a href="#"
                                   class="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out">
                                    Blog
                                </a>
                                <a href="#"
                                   class="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out">
                                    About
                                </a>
                            </div>

                            <div>
                                @auth
                                    <a href="{{ route('home') }}"
                                       class="block w-full px-5 py-3 text-center font-medium text-indigo-600 bg-gray-50 hover:bg-gray-100 hover:text-indigo-700 focus:outline-none focus:bg-gray-100 focus:text-indigo-700 transition duration-150 ease-in-out">
                                        {{ __('Dashboard') }}
                                    </a>
                                @else
                                    <a href="{{ route('login') }}"
                                       class="block w-full px-5 py-3 text-center font-medium text-indigo-600 bg-gray-50 hover:bg-gray-100 hover:text-indigo-700 focus:outline-none focus:bg-gray-100 focus:text-indigo-700 transition duration-150 ease-in-out">
                                        {{ __('Login') }}
                                    </a>
                                @endauth
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-10 mx-auto max-w-screen-xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
                    <div class="sm:text-center lg:text-left">
                        <h2 class="text-4xl tracking-tight leading-10 font-extrabold text-gray-900 sm:text-5xl sm:leading-none md:text-6xl">
                            Jaki będzie Twój
                            <br class="xl:hidden"/>
                            <span class="text-indigo-600">następny kierunek podróży</span>?
                        </h2>
                        <p class="mt-3 text-base text-gray-500 sm:mt-5 sm:text-lg sm:max-w-xl sm:leading-7 sm:mx-auto md:mt-5 md:text-xl lg:mx-0">
                            Curate your bucket list and keep track of your next trips. Search for the most popular
                            destinations on our planet.
                        </p>

                        <div class="mt-5 sm:mt-8 sm:flex sm:justify-center lg:justify-start">
                            <div class="rounded-md shadow">
                                <a href="{{ route('home') }}#search"
                                   class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline transition duration-150 ease-in-out md:py-4 md:text-lg md:px-10">
                                    Skontaktuj się
                                </a>
                            </div>
                            <div class="mt-3 sm:mt-0 sm:ml-3">
                                <a href="{{ route('vehicle.index') }}"
                                   class="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-indigo-700 bg-indigo-100 hover:text-indigo-600 hover:bg-indigo-50 focus:outline-none focus:shadow-outline focus:border-indigo-300 transition duration-150 ease-in-out md:py-4 md:text-lg md:px-10">
                                    Sprawdź auta
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lg:absolute lg:inset-y-0 lg:right-0 lg:w-1/2">
            <img class="h-56 w-full object-cover sm:h-72 md:h-96 lg:w-full lg:h-full" src="/images/car-basic.jpg">
        </div>
    </div>

    <div id="search" class="relative pt-16 pb-20 px-4 sm:px-6 lg:pt-24 lg:pb-28 lg:px-8">
        <div class="absolute inset-0">
            <div class="bg-gray-50 h-1/3 sm:h-2/3"></div>
        </div>
        <div class="relative max-w-7xl mx-auto">
            <div class="text-center">
                <h2 class="text-3xl leading-9 tracking-tight font-extrabold text-gray-900 sm:text-4xl sm:leading-10">
                    Nie znajdziesz u nas marketingowego bełkotu
                </h2>
                <p class="mt-3 max-w-2xl mx-auto text-xl leading-7 text-gray-500 sm:mt-4">
                    Przedstawiamy tylko prawdziwe i najpotrzebniejsze dane wraz z historią auta.
                </p>
{{--                <div class="mt-8">--}}
{{--                    <div class="mt-1 relative rounded-md shadow-sm max-w-2xl mx-auto">--}}
{{--                        <x-input--}}
{{--                            name="search"--}}
{{--                            placeholder="Where to next?"--}}
{{--                            class="form-input block w-full pr-10 sm:text-xl sm:leading-8"--}}
{{--                        />--}}
{{--                        <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">--}}
{{--                            <x-heroicon-s-search class="h-5 w-5 text-gray-400"/>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>

            <div class="mt-12 grid gap-5 max-w-lg mx-auto lg:grid-cols-3 lg:max-w-none">
                <div class="flex flex-col rounded-lg shadow-lg overflow-hidden">
                    <div class="flex-shrink-0 relative">
{{--                        <x-unsplash photo="iFtuhgn7fYs" class="h-48 w-full object-cover"/>--}}

                        <button
                            class="absolute top-0 right-0 mt-2 mr-2 p-2 rounded-md text-gray-400 bg-gray-100 bg-opacity-50 hover:text-red-500 hover:bg-gray-100 hover:bg-opacity-100 focus:outline-none focus:bg-gray-100 focus:text-red-500 transition duration-150 ease-in-out">
                            <x-heroicon-s-heart class="h-6 w-6"/>
                        </button>
                    </div>
                    <div class="flex-1 bg-white p-6 flex flex-col justify-between">
                        <div class="flex-1">
                            <div class="text-sm leading-5 font-medium text-indigo-600">
                                <a href="#" class="hover:underline">
                                    Europe
                                </a>
                            </div>
                            <a href="#" class="block">
                                <h3 class="mt-2 text-xl leading-7 font-semibold text-gray-900">
                                    Berlin, Germany
                                </h3>
                                <p class="mt-3 text-base leading-6 text-gray-500">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto accusantium
                                    praesentium eius, ut atque fuga culpa, similique sequi cum eos quis dolorum.
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="flex flex-col rounded-lg shadow-lg overflow-hidden">
                    <div class="flex-shrink-0 relative">
                        <button
                            class="absolute top-0 right-0 mt-2 mr-2 p-2 rounded-md text-gray-400 bg-gray-100 bg-opacity-50 hover:text-red-500 hover:bg-gray-100 hover:bg-opacity-100 focus:outline-none focus:bg-gray-100 focus:text-red-500 transition duration-150 ease-in-out">
                            <x-heroicon-s-heart class="h-6 w-6"/>
                        </button>
                    </div>
                    <div class="flex-1 bg-white p-6 flex flex-col justify-between">
                        <div class="flex-1">
                            <div class="grid grid-cols-2 gap-4 text-sm leading-5 font-medium text-indigo-600">
                                <a href="#" class="hover:underline">
                                    North America
                                </a>
                            </div>
                            <a href="#" class="block">
                                <h3 class="mt-2 text-xl leading-7 font-semibold text-gray-900">
                                    New York, United States
                                </h3>
                                <p class="mt-3 text-base leading-6 text-gray-500">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit facilis asperiores
                                    porro quaerat doloribus, eveniet dolore. Adipisci tempora aut inventore optio
                                    animi., tempore temporibus quo laudantium.
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="flex flex-col rounded-lg shadow-lg overflow-hidden">
                    <div class="flex-shrink-0 relative">
{{--                        <x-unsplash photo="t9Td0zfDTwI" class="h-48 w-full object-cover"/>--}}

                        <button
                            class="absolute top-0 right-0 mt-2 mr-2 p-2 rounded-md text-gray-400 bg-gray-100 bg-opacity-50 hover:text-red-500 hover:bg-gray-100 hover:bg-opacity-100 focus:outline-none focus:bg-gray-100 focus:text-red-500 transition duration-150 ease-in-out">
                            <x-heroicon-s-heart class="h-6 w-6"/>
                        </button>
                    </div>
                    <div class="flex-1 bg-white p-6 flex flex-col justify-between">
                        <div class="flex-1">
                            <div class="grid grid-cols-2 gap-4 text-sm leading-5 font-medium text-indigo-600">
                                <a href="#" class="hover:underline">
                                    Europe
                                </a>
                            </div>
                            <a href="#" class="block">
                                <h3 class="mt-2 text-xl leading-7 font-semibold text-gray-900">
                                    Paris, France
                                </h3>
                                <p class="mt-3 text-base leading-6 text-gray-500">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint harum rerum voluptatem
                                    quo recusandae magni placeat saepe molestiae, sed excepturi cumque corporis
                                    perferendis hic.
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layouts.base>
