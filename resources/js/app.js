import Alpine from 'alpinejs'
import FormsAlpinePlugin from '../../vendor/filament/forms/dist/module.esm'

import Swup from 'swup';
import SwupLivewirePlugin from '@swup/livewire-plugin';

Alpine.plugin(FormsAlpinePlugin)

window.Alpine = Alpine

Alpine.start()

require('./bootstrap');

localStorage.theme = 'light'

if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
    document.documentElement.classList.add('dark')
} else {
    document.documentElement.classList.remove('dark')
}

// Whenever the user explicitly chooses light mode
// localStorage.theme = 'light'

// Whenever the user explicitly chooses dark mode
// localStorage.theme = 'dark'

// Whenever the user explicitly chooses to respect the OS preference
// localStorage.removeItem('theme')

const swup = new Swup({
    plugins: [new SwupLivewirePlugin()],
    animationSelector: '[class*="page-transition-"]',
    animateHistoryBrowsing: true,
});
