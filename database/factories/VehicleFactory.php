<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Vehicle>
 */
class VehicleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'brand' => $this->faker->vehicleBrand,
            'model' => $this->faker->vehicleModel,
            'mileage' => $this->faker->numberBetween(1, 999),
            'price' => $this->faker->numberBetween(5000, 150000),
            'vin' => $this->faker->vin,
            'description' => [],
        ];
    }
}
